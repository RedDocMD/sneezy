//
// Created by dknite on 6/29/21.
//

#ifndef SNEEZY_MAIN_GUI_H
#define SNEEZY_MAIN_GUI_H

#include <gtk/gtk.h>

void main_gui_activate(GtkApplication *app, gpointer user_data);

#endif //SNEEZY_MAIN_GUI_H
