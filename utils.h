//
// Created by dknite on 6/26/21.
//

#ifndef SNEEZY_UTILS_H
#define SNEEZY_UTILS_H

#include <stdbool.h>
#include <stddef.h>

bool is_git_repo(char *dir);

struct Branches {
	char **names;
	size_t cnt;
};

int get_git_branch_names(char *dir, struct Branches *b);
void free_branch_arr(struct Branches b);

// Appends second to first and returns a *new* string
// Unlike strcat, memory *will* be allocated here.
// So, remember to free it.
char *str_append(const char *first, const char *second);

#endif //SNEEZY_UTILS_H
