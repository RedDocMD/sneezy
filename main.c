#include <stdio.h>
#include <gtk/gtk.h>
#include "utils.h"
#include "main_gui.h"

const char app_name[] = "org.deep.sneezy";
const char resource_root[] = "/org/deep/sneezy";

int main(int argc, char *argv[])
{
	printf("Hello, World!\n");
	char path[] = "/home/dknite/work/llvm-project/llvm";
	if (is_git_repo(path)) {
		struct Branches b;
		size_t i;
		if (get_git_branch_names(path, &b) == 0) {
			for (i = 0; i < b.cnt; ++i)
				printf("%s\n", b.names[i]);
			free_branch_arr(b);
		} else {
			perror("failed to get branch b\n");
			return 1;
		}

		GtkApplication *app;
		app = gtk_application_new(app_name,
					  G_APPLICATION_FLAGS_NONE);
		g_signal_connect(app, "activate", G_CALLBACK(main_gui_activate),
				 NULL);
		g_application_run(G_APPLICATION(app), argc, argv);
		g_object_unref(app);
	} else {
		printf("%s is not a git repo\n", path);
	}
	return 0;
}
