//
// Created by dknite on 6/26/21.
//

#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/errno.h>

bool is_git_repo(char *dir)
{
	size_t dir_len;
	char *git_dir;
	struct stat stat_buf;

	dir_len = strlen(dir);
	git_dir = (char *)malloc(dir_len + 6);
	if (!git_dir) {
		perror("failed to allocate memory for string\n");
		exit(1);
	}
	strcpy(git_dir, dir);
	strcat(git_dir, "/.git");

	if (stat(git_dir, &stat_buf) == 0) {
		free(git_dir);
		return (stat_buf.st_mode & S_IFMT) == S_IFDIR;
	} else {
		free(git_dir);
		if (errno != EACCES) {
			perror("fatal error while checking .git directory\n");
			exit(1);
		}
		return false;
	}
}

int get_git_branch_names(char *dir, struct Branches *b)
{
	size_t dir_len, head_len, sz, cap, i, j;
	char *heads_path, *branches;
	char head[] = ".git/refs/heads";
	DIR *heads_dir;
	struct dirent *entry;

	dir_len = strlen(dir);
	head_len = strlen(head);
	heads_path = (char *)malloc(dir_len + head_len + 2);
	if (!heads_path) {
		perror("failed to allocate memory for string\n");
		exit(1);
	}
	strcpy(heads_path, dir);
	strcat(heads_path, "/");
	strcat(heads_path, head);

	heads_dir = opendir(heads_path);
	free(heads_path);
	if (!heads_dir) {
		perror("failed to open .git/ref/heads\n");
		return -1;
	}

	sz = 0;
	cap = 1024;
	b->cnt = 0;
	branches = (char *)malloc(cap);
	while ((entry = readdir(heads_dir))) {
		size_t new_sz;
		char *buf;

		if (strcmp(entry->d_name, ".") == 0 ||
		    strcmp(entry->d_name, "..") == 0)
			continue;

		new_sz = sz + strlen(entry->d_name) + 1;
		if (new_sz > cap) {
			buf = (char *)realloc(branches, 2 * cap);
			cap *= 2;
			if (!buf) {
				free(branches);
				closedir(heads_dir);
				perror("failed to allocate memory for string\n");
				exit(1);
			}
			branches = buf;
		}
		strcpy(branches + sz, entry->d_name);
		sz = new_sz;
		++(b->cnt);
	}
	closedir(heads_dir);

	b->names = (char **)malloc(b->cnt * sizeof(*(b->names)));
	b->names[0] = branches;
	j = 1;
	for (i = 0; i < sz && j < b->cnt; ++i) {
		if (branches[i] == '\0') {
			b->names[j] = branches + i + 1;
			++j;
		}
	}

	return 0;
}

void free_branch_arr(struct Branches b)
{
	free(*(b.names));
	free(b.names);
}

char *str_append(const char *first, const char *second)
{
	char *buf, *ret;
	size_t i;

	buf = (char *)malloc(strlen(first) + strlen(second) + 1);
	if (!buf) {
		perror("failed to allocate memory for string\n");
		exit(1);
	}
	ret = buf;

	i = 0;
	while (*first) buf[i++] = *(first++);
	while (*second) buf[i++] = *(second++);
	buf[i] = '\0';

	return ret;
}
