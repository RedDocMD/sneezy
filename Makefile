CC ?= gcc
PKGCONFIG = $(shell which pkg-config)
CFLAGS = $(shell $(PKGCONFIG) --cflags gtk4)
LIBS = $(shell $(PKGCONFIG) --libs gtk4)
GLIB_COMPILE_RESOURCES = $(shell $(PKGCONFIG) --variable=glib_compile_resources gio-2.0)

SRC = main.c main_gui.c utils.c
BUILT_SRC = resources.c

OBJS = $(BUILT_SRC:.c=.o) $(SRC:.c=.o)

all: sneezy

resources.c: sneezy.gresource.xml main_gui.ui
	$(GLIB_COMPILE_RESOURCES) sneezy.gresource.xml --target=$@ --sourcedir=. --generate-source

sneezy: $(OBJS)
	$(CC) -o $(@F) $(OBJS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(BUILT_SRC)
	rm -f $(OBJS)
	rm -f sneezy
