//
// Created by dknite on 6/29/21.
//

#include "main_gui.h"
#include "constants.h"
#include "utils.h"

const char main_gui_ui_file[] = "/main_gui.ui";

enum FunctionEnum {
	MASS_REBASE,
	REMOTE_CHECKOUT,
};

struct MainGuiData {
	enum FunctionEnum curr_func;
};

struct MainGuiData *main_data;

gboolean close_request(GtkWindow *window, gpointer user_data)
{
	free(main_data);
	return FALSE;
}

void init_main_data(struct MainGuiData *data)
{
	data->curr_func = MASS_REBASE;
}

void main_gui_activate(GtkApplication *app, gpointer user_data)
{
	GtkBuilder *builder;
	GObject *window;
	char *res_path;

	res_path = str_append(resource_root, main_gui_ui_file);
	builder = gtk_builder_new_from_resource(res_path);
	free(res_path);

	main_data = (struct MainGuiData *)malloc(sizeof(*main_data));
	init_main_data(main_data);

	window = gtk_builder_get_object(builder, "main_window");
	gtk_window_set_application(GTK_WINDOW(window), app);
	g_signal_connect(window, "close_request", G_CALLBACK(close_request),
			 NULL);

	gtk_widget_show(GTK_WIDGET(window));

	g_object_unref(builder);
}